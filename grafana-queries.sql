
SELECT time, usage_user as user, usage_system as system, usage_softirq as softirq, usage_steal as steal, usage_nice as nice, usage_irq as irq, usage_iowait as iowait, usage_guest as guest, usage_guest_nice as guest_nice
FROM $__database.cpu
WHERE host = '$server'
AND cpu = 'cpu-total'
AND $__timeFilter


SELECT time, memory_rss as rss, memory_vms as vms, cpu_usage as cpu_usage
FROM $__database.procstat
WHERE host = '$server'
AND $__timeFilter

SELECT time, total as total, free as free, used_percent as "used %" 
FROM $__database.disk
WHERE host = '$server'
AND $__timeFilter
AND path = '/vol1'

SELECT time, value as "cpu temp" 
FROM $__database.cpu_temp
WHERE host = '$server'
AND $__timeFilter


SELECT temp as temperature, humidity 
FROM $__database.th_1_rdc
WHERE host = '$server'
AND $__timeFilter

SELECT humidity as "humidity" FROM "th_1_rdc" WHERE "host" =~ /$server$/ AND $timeFilter GROUP BY time($interval), *

SELECT temp as "temperature" FROM "th_2_ch_champs" WHERE "host" =~ /$server$/ AND $timeFilter GROUP BY time(20m)

SELECT temp as "temperature" FROM "th_3_ch_rue" WHERE "host" =~ /$server$/ AND $timeFilter GROUP BY time(20m)


SELECT time, download as "download bw", upload as "upload bw", latency
FROM $__database.wan
WHERE host = '$server'
AND $__timeFilter


select upload as "upload bw" from "wan" WHERE "host" =~ /$server$/ AND $timeFilter GROUP BY time($interval), *
select latency as "latency" from "wan" WHERE "host" =~ /$server$/ AND $timeFilter GROUP BY time($interval), *


SELECT time, total as "total processes"
FROM $__database.processes
WHERE host = '$server'
AND $__timeFilter

SELECT time, total as total, used as used, cached as cached, buffered as buffered, shared as shared, swap_cached as swap
FROM $__database.mem
WHERE host = '$server'
AND $__timeFilter

SELECT time, non_negative_derivative(bytes_recv),1s)*8 as 'in'
FROM $__database.net
WHERE host = '$server'
AND $__timeFilter
AND interface =~ /eno1/ 

SELECT time, non_negative_derivative(bytes_sent),1s)*8 as out
FROM $__database.net
WHERE host = '$server'
AND $__timeFilter
AND interface =~ /eno1/ 

