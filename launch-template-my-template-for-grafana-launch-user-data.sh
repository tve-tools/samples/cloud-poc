#!/bin/bash  
yum -y update
#yum install -y docker git python
#systemctl enable docker.service
#systemctl start docker.service
#curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
#chmod +x /usr/local/bin/docker-compose
#mkdir /grafana
#cd /grafana
#curl -s https://gitlab.com/tve-tools/samplesng/-/raw/master/dockerbox/grafana/cloud-docker-compose.yml -o docker-compose.yml
#docker-compose up --detach
#cd ~
#git clone https://gitlab.com/tve-tools/samples/cloud-poc.git
#echo admin >~/.ssh/.pmon
#cloud-poc/my_http_reqs.py

stress --cpu 1 --timeout 15m &

INSTANCE_ID=$(ec2-metadata -i |sed -e 's/.*: //')

echo $INSTANCE_ID

PREV_ALARM=$(aws cloudwatch describe-alarms --alarm-name-prefix cpu-mon |jq -r '.MetricAlarms[].AlarmName' 2>/dev/null)
# aws cloudwatch delete-alarms --alarm-names $PREV_ALARM 2>/dev/null

aws cloudwatch put-metric-alarm --alarm-name "cpu-mon-$INSTANCE_ID" --alarm-description "Alarm when CPU goes is less than 1 percent" --metric-name CPUUtilization --namespace AWS/EC2 --statistic Average --period 300 --threshold 1 --comparison-operator LessThanThreshold  --dimensions "Name=InstanceId,Value=$INSTANCE_ID" --evaluation-periods 2 --datapoints-to-alarm 2 --alarm-actions arn:aws:automate:eu-west-1:ec2:terminate --unit Percent
