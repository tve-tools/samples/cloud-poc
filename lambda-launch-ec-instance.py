import boto3

REGION = 'eu-west-1'
AMI = 'ami-0432c2005d3e6a7f4'
INSTANCE_TYPE = 't3.micro'
LAUNCH_TEMPLATE_ID = 'lt-088c6a7cb85f1cf8a'

EC2 = boto3.client('ec2', region_name=REGION)

def create_launch_template():
  # Create a launch template
    response = EC2.create_launch_template(
        DryRun=False,
        LaunchTemplateName='my-launch-template-1',
        VersionDescription='Initial version',
        LaunchTemplateData={
          'ImageId': AMI,
          'InstanceType': INSTANCE_TYPE,
          'Placement': {
              'AvailabilityZone': REGION + 'a',
          }
        }
    )

    # Get the launch template ID
    launch_template_id = response['LaunchTemplate']['LaunchTemplateId']
    print(launch_template_id)
    print(response)
    return(launch_template_id)


def create_fleet(launch_template_id):
  None
  #create_fleet_inner(launch_template_id)

def create_fleet_inner(launch_template_id):
  launch_template_config = {
    'LaunchTemplateSpecification': {
        'LaunchTemplateId': launch_template_id,
        'Version': '$Latest'
    }
  }

  response = ''
  
  response = EC2.create_fleet(
    DryRun=False,
    TargetCapacitySpecification={
        'TotalTargetCapacity': 1,
        'OnDemandTargetCapacity': 0,
        'SpotTargetCapacity': 1,
        'DefaultTargetCapacityType': 'spot'
    },
    LaunchTemplateConfigs=[launch_template_config],
    Type='request'
  )

  print(response)

def check_running_instance():
  EC2_RESOURCE = boto3.resource('ec2', region_name=REGION)
  # instances = EC2_RESOURCE.instances.all()
  instances = EC2_RESOURCE.instances.filter(Filters=[{'Name': 'instance-state-name', 'Values': ['running', 'pending']}])
  instances_count = len([instance for instance in instances])

  msg = ''
  if instances_count < 1:
    msg = 'about to trigger new instance ...'
    create_fleet(LAUNCH_TEMPLATE_ID)

  else:
    print('use this instance...')
    instance = None
    for inst in instances:
      instance = inst
      break
    if instance.state['Code'] == 16:
      msg = '<a href="http://' + instance.public_ip_address + ':3000">Grafana</a><br>'
    elif instance.state['Code'] == 48:
      msg = 'terminated instance; about to trigger new instance ...'
      create_fleet(LAUNCH_TEMPLATE_ID)

    else:
      msg = 'instance in ' + instance.state['Name']
  
  return(msg)

def lambda_handler(event, context):
  # launch_template_id = create_launch_template()
  # launch_template_id = 'lt-088c6a7cb85f1cf8a'
  msg = check_running_instance()
  return {
    "statusCode": 200,
    "headers": {'Content-Type': 'text/html'},
    "body": msg
  }
