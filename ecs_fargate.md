## aws secrets, aws system manager param-store

[doc at gitlab](https://gitlab.com/tve-tools/samplesng/-/blob/master/dockerbox/ecs_try/aws-readme.md)

## aws ecr

### create private repo from console


### setup docker to push to private repo
```bash
aws@maisel02:~ $ docker tag teggy/graf_conf:latest 273205945833.dkr.ecr.eu-west-1.amazonaws.com/grafana_service:latest
# OR
aws@maisel02:~ $ docker tag 9f881479b55e           273205945833.dkr.ecr.eu-west-1.amazonaws.com/grafana_service:latest

# setup docker to login to aws repo; admin1 should have write access to ECR
aws@maisel02:~ $ AWS_PROFILE=admin1 aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 273205945833.dkr.ecr.eu-west-1.amazonaws.com

# push image to repo
aws@maisel02:~ $ AWS_PROFILE=admin1 docker push 273205945833.dkr.ecr.eu-west-1.amazonaws.com/grafana_service:latest

```
  

