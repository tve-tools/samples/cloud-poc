#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import os
import json



def grafana_load_dashboard(dash_filename):
  with open(dash_filename, "r", encoding="utf-8", errors="surrogateescape") as dash_file:
      data = json.load(dash_file)
      return(data)

#  print(data)

def grafana_add_dashboard():

  headers = { 'Content-Type': 'application/json' }
  passw = ''
  with open(os.path.expanduser('~/.ssh/.pmon')) as inf:
    passw = inf.read().rstrip()

  # get orgid
#  json_data = { 'name':'apiorg' }
#  response = requests.get('http://localhost:3000/api/orgs', auth=('admin', passw), headers=headers, json=json_data)
#  print('response: ' + json.dumps(json.loads(response.text), indent=1))
#
#  orgId = json.loads(response.text)[0]['id']

  # create token
  json_data = { 'name': 'apikeycurl', 'role': 'Admin'}
  response = requests.post('http://localhost:3000/api/auth/keys', headers=headers, json=json_data, auth=('admin', passw))
  json_response = json.loads(response.text)
  print('response: ' + json.dumps(json_response, indent=1))
  key = json_response['key']
#  key = 'sinqlqind'

  # create dashboard
  headers = {
    'Authorization': 'Bearer ' + key,
    'Content-Type': 'application/json',
  }

  json_data = {
    'dashboard': grafana_load_dashboard(os.path.expanduser(os.path.dirname(__file__) + '/grafana-dash.json')),
    'overwrite': True,
  }

  response = requests.post('http://localhost:3000/api/dashboards/db', headers=headers, json=json_data, verify=False)
  print('response: ' + json.dumps(json.loads(response.text), indent=1))

grafana_add_dashboard()

