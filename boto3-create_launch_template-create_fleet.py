import boto3

def lambda_handler(event, context):
    ec2 = boto3.client('ec2')

    # Create a launch template
    response = ec2.create_launch_template(
        DryRun=False,
        LaunchTemplateName='my-launch-template-1',
        VersionDescription='Initial version',
        LaunchTemplateData={
            'ImageId': 'ami-02b20e5594a5e5398',
            'InstanceType': 't2.medium',
            'Placement': {
                'AvailabilityZone': 'us-east-1a',
            },
            'EbsOptimized': True,
            'Monitoring': {
                'Enabled': True
            },
            'SecurityGroupIds': [
                'sg-053a39faea8548b14',
            ]
        }
    )

    # Get the launch template ID
    launch_template_id = response['LaunchTemplate']['LaunchTemplateId']

    # Set the desired capacity of the fleet to the number of instance types specified
    desired_capacity = 6

    # Set the target capacity of the fleet to the desired capacity
    target_capacity = desired_capacity

    # Create a launch template configuration for each instance type in the fleet
    launch_template_configs = []
    instance_types = ['t2.medium', 't2.large', 't3a.medium', 't3a.large', 't3.medium', 't3.large']
    for instance_type in instance_types:
        launch_template_config = {
            'LaunchTemplateSpecification': {
                'LaunchTemplateId': launch_template_id,
                'Version': '$Latest'
            },
            'Overrides': [
                {
                    'InstanceType': instance_type
                }
            ]
        }
        launch_template_configs.append(launch_template_config)

    # Create the fleet
    response = ec2.create_fleet(
        DryRun=False,
        TargetCapacitySpecification={
            'TotalTargetCapacity': target_capacity,
            'OnDemandTargetCapacity': 0,
            'SpotTargetCapacity': target_capacity,
            'DefaultTargetCapacityType': 'spot'
        },
        LaunchTemplateConfigs=launch_template_configs
    )

    print(response)
